﻿using Data.Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class User : BaseEntity
    {
        /// <summary>
        /// 
        /// </summary>        
        public User()
        {
            Role = UserType.MANAGER;
        }

        /// <summary>
        /// Gets or sets user role type.
        /// </summary>
        public UserType Role { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(AllowEmptyStrings=false)]
        [StringLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [NotMapped]
        public virtual string RoleName
        {
            get { return Enum.GetName(typeof(UserType), Role); }
        }

        public virtual IEnumerable<Sale> Sales { get; set; }

        public virtual IEnumerable<StoreItem> StoreItems { get; set; }
        
    }
}
