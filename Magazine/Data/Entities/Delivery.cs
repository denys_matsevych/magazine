﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Data.Entities
{
    public class Delivery : BaseEntity
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string Name { get; set; }

        public virtual IEnumerable<StoreItem> StoreItems { get; set; }
    }
}
