﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Data.Entities
{
    public class Sale : BaseEntity
    {
        public double Quantity { get; set; }

        public double TotalPrice { get; set; }

        public DateTime SaleDate { get; set; }

        [Required]
        public virtual Product Product { get; set; }

        [Required]
        public virtual User User { get; set; }

    }
}
