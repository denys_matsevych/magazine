﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Data.Entities
{
    public class Unit : BaseEntity
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(10)]
        public string Measurement { get; set; }

        public virtual IEnumerable<Product> Products { get; set; }
    }
}
