﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Data.Entities
{
    public class StoreItem : BaseEntity
    {
        public DateTime DeliveryDate { get; set; }

        public double TotalAmount { get; set; }

        [Required]
        public virtual Product Product { get; set; }

        [Required]
        public virtual User User { get; set; }

        [Required]
        public virtual Delivery Delivery { get; set; }
    }
}
