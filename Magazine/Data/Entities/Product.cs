﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Data.Entities
{
    public class Product : BaseEntity
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string Name { get; set; }

        public double Quantity { get; set; }

        public double Price { get; set; }

        [Required]
        public virtual Unit Unit { get; set; }

        public virtual IEnumerable<StoreItem> StoreItems { get; set; }

        public virtual IEnumerable<Sale> Sales { get; set; }
    }
}
