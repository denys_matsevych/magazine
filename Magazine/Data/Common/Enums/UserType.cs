﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Common.Enums
{
    /// <summary>
    /// 
    /// </summary>
    public enum UserType
    {
        /// <summary>
        /// 
        /// </summary>
        CASHIER = 0,

        /// <summary>
        /// 
        /// </summary>
        STORE_KEAPER = 1,

        /// <summary>
        /// 
        /// </summary>
        MANAGER = 2
    }
}
