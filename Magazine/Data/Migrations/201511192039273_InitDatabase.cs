namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Deliveries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Quantity = c.Double(nullable: false),
                        Price = c.Double(nullable: false),
                        Unit_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Units", t => t.Unit_Id)
                .Index(t => t.Unit_Id);
            
            CreateTable(
                "dbo.Units",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Measurement = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Sales",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Quantity = c.Double(nullable: false),
                        TotalPrice = c.Double(nullable: false),
                        SaleDate = c.DateTime(nullable: false),
                        Product_Id = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.Product_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.Product_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Role = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StoreItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DeliveryDate = c.DateTime(nullable: false),
                        TotalAmount = c.Double(nullable: false),
                        Delivery_Id = c.Int(),
                        Product_Id = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Deliveries", t => t.Delivery_Id)
                .ForeignKey("dbo.Products", t => t.Product_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.Delivery_Id)
                .Index(t => t.Product_Id)
                .Index(t => t.User_Id);

            Sql(@"INSERT INTO Users(Role, Name) VALUES  (0, 'cashier_1'),
                                                        (0, 'cashier_2'),
                                                        (2, 'manager_1'),
                                                        (1, 'storekeaper_1'),
                                                        (1, 'storekeaper_2')
                 ");

            Sql(@"INSERT INTO Units(Measurement) VALUES ('g'), ('kg')");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StoreItems", "User_Id", "dbo.Users");
            DropForeignKey("dbo.StoreItems", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.StoreItems", "Delivery_Id", "dbo.Deliveries");
            DropForeignKey("dbo.Sales", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Sales", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.Products", "Unit_Id", "dbo.Units");
            DropIndex("dbo.StoreItems", new[] { "User_Id" });
            DropIndex("dbo.StoreItems", new[] { "Product_Id" });
            DropIndex("dbo.StoreItems", new[] { "Delivery_Id" });
            DropIndex("dbo.Sales", new[] { "User_Id" });
            DropIndex("dbo.Sales", new[] { "Product_Id" });
            DropIndex("dbo.Products", new[] { "Unit_Id" });
            DropTable("dbo.StoreItems");
            DropTable("dbo.Users");
            DropTable("dbo.Sales");
            DropTable("dbo.Units");
            DropTable("dbo.Products");
            DropTable("dbo.Deliveries");
        }
    }
}
