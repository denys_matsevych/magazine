namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStringsMaxLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Deliveries", "Name", c => c.String(maxLength: 50));
            AlterColumn("dbo.Products", "Name", c => c.String(maxLength: 50));
            AlterColumn("dbo.Units", "Measurement", c => c.String(maxLength: 10));
            AlterColumn("dbo.Users", "Name", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Name", c => c.String());
            AlterColumn("dbo.Units", "Measurement", c => c.String());
            AlterColumn("dbo.Products", "Name", c => c.String());
            AlterColumn("dbo.Deliveries", "Name", c => c.String());
        }
    }
}
