namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRequriedFields : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Products", "Unit_Id", "dbo.Units");
            DropForeignKey("dbo.Sales", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.Sales", "User_Id", "dbo.Users");
            DropForeignKey("dbo.StoreItems", "Delivery_Id", "dbo.Deliveries");
            DropForeignKey("dbo.StoreItems", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.StoreItems", "User_Id", "dbo.Users");
            DropIndex("dbo.Products", new[] { "Unit_Id" });
            DropIndex("dbo.Sales", new[] { "Product_Id" });
            DropIndex("dbo.Sales", new[] { "User_Id" });
            DropIndex("dbo.StoreItems", new[] { "Delivery_Id" });
            DropIndex("dbo.StoreItems", new[] { "Product_Id" });
            DropIndex("dbo.StoreItems", new[] { "User_Id" });
            AlterColumn("dbo.Deliveries", "Name", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Products", "Name", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Products", "Unit_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Units", "Measurement", c => c.String(nullable: false, maxLength: 10));
            AlterColumn("dbo.Sales", "Product_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Sales", "User_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Users", "Name", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.StoreItems", "Delivery_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.StoreItems", "Product_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.StoreItems", "User_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Products", "Unit_Id");
            CreateIndex("dbo.Sales", "Product_Id");
            CreateIndex("dbo.Sales", "User_Id");
            CreateIndex("dbo.StoreItems", "Delivery_Id");
            CreateIndex("dbo.StoreItems", "Product_Id");
            CreateIndex("dbo.StoreItems", "User_Id");
            AddForeignKey("dbo.Products", "Unit_Id", "dbo.Units", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Sales", "Product_Id", "dbo.Products", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Sales", "User_Id", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.StoreItems", "Delivery_Id", "dbo.Deliveries", "Id", cascadeDelete: true);
            AddForeignKey("dbo.StoreItems", "Product_Id", "dbo.Products", "Id", cascadeDelete: true);
            AddForeignKey("dbo.StoreItems", "User_Id", "dbo.Users", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StoreItems", "User_Id", "dbo.Users");
            DropForeignKey("dbo.StoreItems", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.StoreItems", "Delivery_Id", "dbo.Deliveries");
            DropForeignKey("dbo.Sales", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Sales", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.Products", "Unit_Id", "dbo.Units");
            DropIndex("dbo.StoreItems", new[] { "User_Id" });
            DropIndex("dbo.StoreItems", new[] { "Product_Id" });
            DropIndex("dbo.StoreItems", new[] { "Delivery_Id" });
            DropIndex("dbo.Sales", new[] { "User_Id" });
            DropIndex("dbo.Sales", new[] { "Product_Id" });
            DropIndex("dbo.Products", new[] { "Unit_Id" });
            AlterColumn("dbo.StoreItems", "User_Id", c => c.Int());
            AlterColumn("dbo.StoreItems", "Product_Id", c => c.Int());
            AlterColumn("dbo.StoreItems", "Delivery_Id", c => c.Int());
            AlterColumn("dbo.Users", "Name", c => c.String(maxLength: 50));
            AlterColumn("dbo.Sales", "User_Id", c => c.Int());
            AlterColumn("dbo.Sales", "Product_Id", c => c.Int());
            AlterColumn("dbo.Units", "Measurement", c => c.String(maxLength: 10));
            AlterColumn("dbo.Products", "Unit_Id", c => c.Int());
            AlterColumn("dbo.Products", "Name", c => c.String(maxLength: 50));
            AlterColumn("dbo.Deliveries", "Name", c => c.String(maxLength: 50));
            CreateIndex("dbo.StoreItems", "User_Id");
            CreateIndex("dbo.StoreItems", "Product_Id");
            CreateIndex("dbo.StoreItems", "Delivery_Id");
            CreateIndex("dbo.Sales", "User_Id");
            CreateIndex("dbo.Sales", "Product_Id");
            CreateIndex("dbo.Products", "Unit_Id");
            AddForeignKey("dbo.StoreItems", "User_Id", "dbo.Users", "Id");
            AddForeignKey("dbo.StoreItems", "Product_Id", "dbo.Products", "Id");
            AddForeignKey("dbo.StoreItems", "Delivery_Id", "dbo.Deliveries", "Id");
            AddForeignKey("dbo.Sales", "User_Id", "dbo.Users", "Id");
            AddForeignKey("dbo.Sales", "Product_Id", "dbo.Products", "Id");
            AddForeignKey("dbo.Products", "Unit_Id", "dbo.Units", "Id");
        }
    }
}
