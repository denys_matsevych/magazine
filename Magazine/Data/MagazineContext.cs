﻿using Data.Entities;
using Data.Migrations;
using System.Data.Entity;

namespace Data
{
    public class MagazineContext : DbContext
    {
        public MagazineContext()
            : base("name=MagazineDbConnection")
        {
            Database.SetInitializer<MagazineContext>(new MigrateDatabaseToLatestVersion<MagazineContext, Configuration>());
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<Sale> Sales { get; set; }
        public DbSet<StoreItem> StoreItems { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Delivery> Deliveries { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
