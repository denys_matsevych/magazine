﻿namespace Magazine.UI
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvStoreInfo = new System.Windows.Forms.DataGridView();
            this.btnDeleteStoreRow = new System.Windows.Forms.Button();
            this.btnAddStoreItem = new System.Windows.Forms.Button();
            this.btnRefreshTableStore = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStoreInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvStoreInfo
            // 
            this.dgvStoreInfo.AllowUserToAddRows = false;
            this.dgvStoreInfo.AllowUserToDeleteRows = false;
            this.dgvStoreInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStoreInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvStoreInfo.Location = new System.Drawing.Point(0, 0);
            this.dgvStoreInfo.Name = "dgvStoreInfo";
            this.dgvStoreInfo.ReadOnly = true;
            this.dgvStoreInfo.Size = new System.Drawing.Size(1204, 380);
            this.dgvStoreInfo.TabIndex = 0;
            // 
            // btnDeleteStoreRow
            // 
            this.btnDeleteStoreRow.Location = new System.Drawing.Point(148, 421);
            this.btnDeleteStoreRow.Name = "btnDeleteStoreRow";
            this.btnDeleteStoreRow.Size = new System.Drawing.Size(130, 40);
            this.btnDeleteStoreRow.TabIndex = 6;
            this.btnDeleteStoreRow.Text = "Delete";
            this.btnDeleteStoreRow.UseVisualStyleBackColor = true;
            // 
            // btnAddStoreItem
            // 
            this.btnAddStoreItem.Location = new System.Drawing.Point(12, 421);
            this.btnAddStoreItem.Name = "btnAddStoreItem";
            this.btnAddStoreItem.Size = new System.Drawing.Size(130, 40);
            this.btnAddStoreItem.TabIndex = 5;
            this.btnAddStoreItem.Text = "Add";
            this.btnAddStoreItem.UseVisualStyleBackColor = true;
            // 
            // btnRefreshTableStore
            // 
            this.btnRefreshTableStore.Location = new System.Drawing.Point(1062, 421);
            this.btnRefreshTableStore.Name = "btnRefreshTableStore";
            this.btnRefreshTableStore.Size = new System.Drawing.Size(130, 40);
            this.btnRefreshTableStore.TabIndex = 4;
            this.btnRefreshTableStore.Text = "Refresh";
            this.btnRefreshTableStore.UseVisualStyleBackColor = true;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1204, 461);
            this.Controls.Add(this.btnDeleteStoreRow);
            this.Controls.Add(this.btnAddStoreItem);
            this.Controls.Add(this.btnRefreshTableStore);
            this.Controls.Add(this.dgvStoreInfo);
            this.MaximizeBox = false;
            this.Name = "Form3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form : : STOREKEAPER ";
            ((System.ComponentModel.ISupportInitialize)(this.dgvStoreInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvStoreInfo;
        private System.Windows.Forms.Button btnDeleteStoreRow;
        private System.Windows.Forms.Button btnAddStoreItem;
        private System.Windows.Forms.Button btnRefreshTableStore;
    }
}