﻿namespace Magazine.UI
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvTableSales = new System.Windows.Forms.DataGridView();
            this.btnRefreshTableSales = new System.Windows.Forms.Button();
            this.btnAddSaleRow = new System.Windows.Forms.Button();
            this.btnDeleteSaleRow = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTableSales)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvTableSales
            // 
            this.dgvTableSales.AllowUserToAddRows = false;
            this.dgvTableSales.AllowUserToDeleteRows = false;
            this.dgvTableSales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTableSales.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvTableSales.Location = new System.Drawing.Point(0, 0);
            this.dgvTableSales.Name = "dgvTableSales";
            this.dgvTableSales.ReadOnly = true;
            this.dgvTableSales.Size = new System.Drawing.Size(1204, 380);
            this.dgvTableSales.TabIndex = 0;
            // 
            // btnRefreshTableSales
            // 
            this.btnRefreshTableSales.Location = new System.Drawing.Point(1062, 420);
            this.btnRefreshTableSales.Name = "btnRefreshTableSales";
            this.btnRefreshTableSales.Size = new System.Drawing.Size(130, 40);
            this.btnRefreshTableSales.TabIndex = 1;
            this.btnRefreshTableSales.Text = "Refresh";
            this.btnRefreshTableSales.UseVisualStyleBackColor = true;
            // 
            // btnAddSaleRow
            // 
            this.btnAddSaleRow.Location = new System.Drawing.Point(12, 420);
            this.btnAddSaleRow.Name = "btnAddSaleRow";
            this.btnAddSaleRow.Size = new System.Drawing.Size(130, 40);
            this.btnAddSaleRow.TabIndex = 2;
            this.btnAddSaleRow.Text = "Add";
            this.btnAddSaleRow.UseVisualStyleBackColor = true;
            // 
            // btnDeleteSaleRow
            // 
            this.btnDeleteSaleRow.Location = new System.Drawing.Point(148, 420);
            this.btnDeleteSaleRow.Name = "btnDeleteSaleRow";
            this.btnDeleteSaleRow.Size = new System.Drawing.Size(130, 40);
            this.btnDeleteSaleRow.TabIndex = 3;
            this.btnDeleteSaleRow.Text = "Delete";
            this.btnDeleteSaleRow.UseVisualStyleBackColor = true;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1204, 461);
            this.Controls.Add(this.btnDeleteSaleRow);
            this.Controls.Add(this.btnAddSaleRow);
            this.Controls.Add(this.btnRefreshTableSales);
            this.Controls.Add(this.dgvTableSales);
            this.MaximizeBox = false;
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form : : CASHIER";
            ((System.ComponentModel.ISupportInitialize)(this.dgvTableSales)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvTableSales;
        private System.Windows.Forms.Button btnRefreshTableSales;
        private System.Windows.Forms.Button btnAddSaleRow;
        private System.Windows.Forms.Button btnDeleteSaleRow;
    }
}